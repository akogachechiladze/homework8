package com.example.usaid041021

import java.security.acl.Owner

data class Item(val image: Int, val title: String, val price: String, val owner: Owner? = null) {

    data class Owner(val image: Int, val title: String, val price: String)

}


