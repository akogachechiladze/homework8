package com.example.usaid041021

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.usaid041021.databinding.FirstItemLayoutBinding
import com.example.usaid041021.databinding.SecondItemLayoutBinding

typealias longClick = (item: Item, position: Int) ->Unit

interface ItemClick{
    fun onClick()
}

class ItemsAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val FIRST_ITEM = 10
        private const val SECOND_ITEM = 11
    }

    private val items = mutableListOf<Item>()
    var callBack: ItemClick? = null
    var longClick: longClick? = null



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == FIRST_ITEM) {
            FirstViewHolder(FirstItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        } else {
            SecondViewHolder(SecondItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FirstViewHolder) {
            holder.onBind()
        }else if (holder is SecondViewHolder) {
            holder.onBind()
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (items[position].owner != null)
            return SECOND_ITEM
        else
            return FIRST_ITEM
    }

    override fun getItemCount(): Int {
        return items.size
    }


    inner class FirstViewHolder(val binding: FirstItemLayoutBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener, View.OnLongClickListener {
        private lateinit var model: Item
        fun onBind() {
            model = items[adapterPosition]
            binding.imageFirst.setImageResource(model.image)
            binding.titleFirst.text = model.title
            binding.priceFirst.text = model.price
            binding.root.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            callBack?.onClick()
        }

        override fun onLongClick(v: View?): Boolean {
            delete(adapterPosition)
            longClick?.invoke(model, adapterPosition)
            return true
        }
    }

    inner class SecondViewHolder(val binding: SecondItemLayoutBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener, View.OnLongClickListener{
        private lateinit var model: Item
        fun onBind() {
            model = items[adapterPosition]
            binding.imageSecond.setImageResource(model.owner!!.image)
            binding.titleSecond.text = model.owner!!.title
            binding.priceSecond.text = model.owner!!.price
            binding.root.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            callBack?.onClick()
        }

        override fun onLongClick(v: View?): Boolean {
            longClick?.invoke(model, adapterPosition)
            delete(adapterPosition)
            return true
        }


    }

    fun setData(items: MutableList<Item>) {
        this.items.clear()
        this.items.addAll(this.items)
        notifyDataSetChanged()
    }

    fun Add(newitem: Item) {
        items.add(newitem)
        notifyDataSetChanged()
    }

    fun delete(position: Int){
        this.items.removeAt(position)
        notifyItemRemoved(position)
    }



}