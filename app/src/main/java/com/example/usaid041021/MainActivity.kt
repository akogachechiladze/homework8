package com.example.usaid041021

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import com.example.usaid041021.databinding.ActivityMainBinding




class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var adapter: ItemsAdapter

    val items = mutableListOf<Item>()

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        setData()
        binding.RecyclerView.layoutManager = GridLayoutManager(applicationContext, 2)
        adapter = ItemsAdapter()
        binding.RecyclerView.adapter = adapter
        adapter.callBack = object : ItemClick {
            override fun onClick() {
                TODO("Not yet implemented")
            }

        }

        adapter.setData(items)


        adapter.longClick = { item, position ->
            items.removeAt(position)
        }

        binding.addbutton.setOnClickListener {
            addCard()
        }


    }


    private fun setData(): MutableList<Item> {


        items.add(Item(R.mipmap.google_pixel, "Google Pixel 4XL New", "520"))

        items.add(Item(R.mipmap.google_pixel, "Google Pixel 4XL Refurbished", "460"))

        items.add(Item(R.mipmap.google_pixel, "Google Pixel", "520", Item.Owner(R.mipmap.oneplus, "Oneplus 8T New", "500")))

        items.add(Item(R.mipmap.google_pixel, "Google Pixel", "520", Item.Owner(R.mipmap.oneplus, "Oneplus 8T Used", "380")))

        items.add(Item(R.mipmap.google_pixel, "Google Pixel 4XL Used", "410"))

        items.add(Item(R.mipmap.google_pixel, "Google Pixel", "520", Item.Owner(R.mipmap.oneplus, "Oneplus 8T New", "500")))

        items.add(Item(R.mipmap.google_pixel, "Google Pixel 4XL T-mobile", "490"))

        items.add(Item(R.mipmap.google_pixel, "Google Pixel", "520", Item.Owner(R.mipmap.oneplus, "Oneplus 8T Refurbished", "430")))

        items.add(Item(R.mipmap.google_pixel, "Google Pixel 4XL Used", "390"))

        items.add(Item(R.mipmap.google_pixel, "Google Pixel", "520", Item.Owner(R.mipmap.oneplus, "Oneplus 8T", "500")))

        items.add(Item(R.mipmap.google_pixel, "Google Pixel", "520", Item.Owner(R.mipmap.oneplus, "Oneplus 8T Used", "400")))

        items.add(Item(R.mipmap.google_pixel, "Google Pixel", "520", Item.Owner(R.mipmap.oneplus, "Oneplus 8T New", "500")))

        items.add(Item(R.mipmap.google_pixel, "Google Pixel 4XL", "520"))



        return items



    }

    private fun addCard() {
        if (binding.phoneET.text.toString() == "Oneplus") {
            adapter.Add((Item(R.mipmap.google_pixel, binding.titleET.text.toString(), binding.priceET.text.toString(), Item.Owner(R.mipmap.oneplus, binding.titleET.text.toString(), binding.priceET.text.toString()))))
        } else if (binding.phoneET.text.toString() == "Pixel") {
            adapter.Add(Item(R.mipmap.google_pixel, binding.titleET.text.toString(), binding.priceET.text.toString()))
        }
    }




}



